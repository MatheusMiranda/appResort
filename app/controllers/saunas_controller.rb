class SaunasController < ApplicationController
  before_action :set_sauna, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /saunas
  # GET /saunas.json
  def index
    @saunas = Sauna.all
  end

  # GET /saunas/1
  # GET /saunas/1.json
  def show
  end

  # GET /saunas/new
  def new
    @sauna = Sauna.new
  end

  # GET /saunas/1/edit
  def edit
  end

  # POST /saunas
  # POST /saunas.json
  def create
    @sauna = Sauna.new(sauna_params)

    respond_to do |format|
      if @sauna.save
        format.html { redirect_to @sauna }
        format.json { render :show, status: :created, location: @sauna }
      else
        format.html { render :new }
        format.json { render json: @sauna.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /saunas/1
  # PATCH/PUT /saunas/1.json
  def update
    respond_to do |format|
      if @sauna.update(sauna_params)
        format.html { redirect_to @sauna }
        format.json { render :show, status: :ok, location: @sauna }
      else
        format.html { render :edit }
        format.json { render json: @sauna.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /saunas/1
  # DELETE /saunas/1.json
  def destroy
    @sauna.destroy
    respond_to do |format|
      format.html { redirect_to saunas_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sauna
      @sauna = Sauna.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sauna_params
      params.require(:sauna).permit(:date, :idUser)
    end
end
