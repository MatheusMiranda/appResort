class BarbecueGrillsController < ApplicationController
  before_action :set_barbecue_grill, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /barbecue_grills
  # GET /barbecue_grills.json
  def index
    @barbecue_grills = BarbecueGrill.all
  end

  # GET /barbecue_grills/1
  # GET /barbecue_grills/1.json
  def show
  end

  # GET /barbecue_grills/new
  def new
    @barbecue_grill = BarbecueGrill.new
  end

  # GET /barbecue_grills/1/edit
  def edit
  end

  # POST /barbecue_grills
  # POST /barbecue_grills.json
  def create
    @barbecue_grill = BarbecueGrill.new(barbecue_grill_params)

    respond_to do |format|
      if @barbecue_grill.save
        format.html { redirect_to @barbecue_grill }
        format.json { render :show, status: :created, location: @barbecue_grill }
      else
        format.html { render :new }
        format.json { render json: @barbecue_grill.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /barbecue_grills/1
  # PATCH/PUT /barbecue_grills/1.json
  def update
    respond_to do |format|
      if @barbecue_grill.update(barbecue_grill_params)
        format.html { redirect_to @barbecue_grill }
        format.json { render :show, status: :ok, location: @barbecue_grill }
      else
        format.html { render :edit }
        format.json { render json: @barbecue_grill.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /barbecue_grills/1
  # DELETE /barbecue_grills/1.json
  def destroy
    @barbecue_grill.destroy
    respond_to do |format|
      format.html { redirect_to barbecue_grills_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_barbecue_grill
      @barbecue_grill = BarbecueGrill.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def barbecue_grill_params
      params.require(:barbecue_grill).permit(:date, :idUser)
    end
end
