class KidsPoolsController < ApplicationController
  before_action :set_kids_pool, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /kids_pools
  # GET /kids_pools.json
  def index
    @kids_pools = KidsPool.all
  end

  # GET /kids_pools/1
  # GET /kids_pools/1.json
  def show
  end

  # GET /kids_pools/new
  def new
    @kids_pool = KidsPool.new
  end

  # GET /kids_pools/1/edit
  def edit
  end

  # POST /kids_pools
  # POST /kids_pools.json
  def create
    @kids_pool = KidsPool.new(kids_pool_params)

    respond_to do |format|
      if @kids_pool.save
        format.html { redirect_to @kids_pool }
        format.json { render :show, status: :created, location: @kids_pool }
      else
        format.html { render :new }
        format.json { render json: @kids_pool.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /kids_pools/1
  # PATCH/PUT /kids_pools/1.json
  def update
    respond_to do |format|
      if @kids_pool.update(kids_pool_params)
        format.html { redirect_to @kids_pool }
        format.json { render :show, status: :ok, location: @kids_pool }
      else
        format.html { render :edit }
        format.json { render json: @kids_pool.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /kids_pools/1
  # DELETE /kids_pools/1.json
  def destroy
    @kids_pool.destroy
    respond_to do |format|
      format.html { redirect_to kids_pools_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_kids_pool
      @kids_pool = KidsPool.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def kids_pool_params
      params.require(:kids_pool).permit(:date, :idUser)
    end
end
