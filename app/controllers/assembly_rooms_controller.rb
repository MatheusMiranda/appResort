class AssemblyRoomsController < ApplicationController
  before_action :set_assembly_room, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /assembly_rooms
  # GET /assembly_rooms.json
  def index
    @assembly_rooms = AssemblyRoom.all
  end

  # GET /assembly_rooms/1
  # GET /assembly_rooms/1.json
  def show
  end

  # GET /assembly_rooms/new
  def new
    @assembly_room = AssemblyRoom.new
  end

  # GET /assembly_rooms/1/edit
  def edit
  end

  # POST /assembly_rooms
  # POST /assembly_rooms.json
  def create
    @assembly_room = AssemblyRoom.new(assembly_room_params)

    respond_to do |format|
      if @assembly_room.save
        format.html { redirect_to @assembly_room }
        format.json { render :show, status: :created, location: @assembly_room }
      else
        format.html { render :new }
        format.json { render json: @assembly_room.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /assembly_rooms/1
  # PATCH/PUT /assembly_rooms/1.json
  def update
    respond_to do |format|
      if @assembly_room.update(assembly_room_params)
        format.html { redirect_to @assembly_room }
        format.json { render :show, status: :ok, location: @assembly_room }
      else
        format.html { render :edit }
        format.json { render json: @assembly_room.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /assembly_rooms/1
  # DELETE /assembly_rooms/1.json
  def destroy
    @assembly_room.destroy
    respond_to do |format|
      format.html { redirect_to assembly_rooms_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_assembly_room
      @assembly_room = AssemblyRoom.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def assembly_room_params
      params.require(:assembly_room).permit(:date, :idUser)
    end
end
