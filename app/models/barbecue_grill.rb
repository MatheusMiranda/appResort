class BarbecueGrill < ActiveRecord::Base
	validates :idUser, presence: true
	validates :idUser, uniqueness: true
	validates :date, presence: true
	validates :date, uniqueness: true
end
