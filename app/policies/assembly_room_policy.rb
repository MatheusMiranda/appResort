class AssemblyRoomPolicy < ApplicationPolicy
	
  def index?
  	user.age >= 18
  end

  class Scope < Scope
    def resolve
      scope
    end
  end
end
