class GymPolicy < ApplicationPolicy

  def index?
  	user.age >= 15
  end

  def index2?
  	user.admin?
  end

  class Scope < Scope
    def resolve
      scope
    end
  end
end
