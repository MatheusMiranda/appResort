class AdultPoolPolicy < ApplicationPolicy

  def index?
  	user.age >= 18
  end

  def index2?
  	user.admin?
  end

  class Scope < Scope
    def resolve
      scope
    end
  end
end
