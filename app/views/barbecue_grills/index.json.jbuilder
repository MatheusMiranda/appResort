json.array!(@barbecue_grills) do |barbecue_grill|
  json.extract! barbecue_grill, :id, :date, :idUser
  json.url barbecue_grill_url(barbecue_grill, format: :json)
end
