json.array!(@saunas) do |sauna|
  json.extract! sauna, :id, :date, :idUser
  json.url sauna_url(sauna, format: :json)
end
