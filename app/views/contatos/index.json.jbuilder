json.array!(@contatos) do |contato|
  json.extract! contato, :id, :name, :email, :message
  json.url contato_url(contato, format: :json)
end
