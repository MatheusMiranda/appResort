json.array!(@courts) do |court|
  json.extract! court, :id, :date, :idUser
  json.url court_url(court, format: :json)
end
