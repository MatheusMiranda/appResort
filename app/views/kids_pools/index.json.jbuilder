json.array!(@kids_pools) do |kids_pool|
  json.extract! kids_pool, :id, :date, :idUser
  json.url kids_pool_url(kids_pool, format: :json)
end
