json.array!(@assembly_rooms) do |assembly_room|
  json.extract! assembly_room, :id, :date, :idUser
  json.url assembly_room_url(assembly_room, format: :json)
end
