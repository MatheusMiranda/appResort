class CreateAdultPools < ActiveRecord::Migration
  def change
    create_table :adult_pools do |t|
      t.string :idUser

      t.timestamps null: false
    end
  end
end
