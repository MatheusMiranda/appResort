class CreateBarbecueGrills < ActiveRecord::Migration
  def change
    create_table :barbecue_grills do |t|
      t.datetime :date
      t.string :idUser

      t.timestamps null: false
    end
  end
end
