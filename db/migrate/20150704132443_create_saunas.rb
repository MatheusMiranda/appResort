class CreateSaunas < ActiveRecord::Migration
  def change
    create_table :saunas do |t|
      t.date :date
      t.string :idUser

      t.timestamps null: false
    end
  end
end
