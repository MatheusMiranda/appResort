class CreateGyms < ActiveRecord::Migration
  def change
    create_table :gyms do |t|
      t.date :date
      t.string :idUser

      t.timestamps null: false
    end
  end
end
