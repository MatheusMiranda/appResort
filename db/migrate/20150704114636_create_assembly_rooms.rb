class CreateAssemblyRooms < ActiveRecord::Migration
  def change
    create_table :assembly_rooms do |t|
      t.datetime :date
      t.string :idUser

      t.timestamps null: false
    end
  end
end
