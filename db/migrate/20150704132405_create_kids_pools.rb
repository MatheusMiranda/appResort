class CreateKidsPools < ActiveRecord::Migration
  def change
    create_table :kids_pools do |t|
      t.string :idUser

      t.timestamps null: false
    end
  end
end
