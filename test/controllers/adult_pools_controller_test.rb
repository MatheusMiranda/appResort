require 'test_helper'

class AdultPoolsControllerTest < ActionController::TestCase
  setup do
    @adult_pool = adult_pools(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:adult_pools)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create adult_pool" do
    assert_difference('AdultPool.count') do
      post :create, adult_pool: { date: @adult_pool.date, idUser: @adult_pool.idUser }
    end

    assert_redirected_to adult_pool_path(assigns(:adult_pool))
  end

  test "should show adult_pool" do
    get :show, id: @adult_pool
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @adult_pool
    assert_response :success
  end

  test "should update adult_pool" do
    patch :update, id: @adult_pool, adult_pool: { date: @adult_pool.date, idUser: @adult_pool.idUser }
    assert_redirected_to adult_pool_path(assigns(:adult_pool))
  end

  test "should destroy adult_pool" do
    assert_difference('AdultPool.count', -1) do
      delete :destroy, id: @adult_pool
    end

    assert_redirected_to adult_pools_path
  end
end
