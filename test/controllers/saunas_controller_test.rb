require 'test_helper'

class SaunasControllerTest < ActionController::TestCase
  setup do
    @sauna = saunas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:saunas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sauna" do
    assert_difference('Sauna.count') do
      post :create, sauna: { date: @sauna.date, idUser: @sauna.idUser }
    end

    assert_redirected_to sauna_path(assigns(:sauna))
  end

  test "should show sauna" do
    get :show, id: @sauna
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sauna
    assert_response :success
  end

  test "should update sauna" do
    patch :update, id: @sauna, sauna: { date: @sauna.date, idUser: @sauna.idUser }
    assert_redirected_to sauna_path(assigns(:sauna))
  end

  test "should destroy sauna" do
    assert_difference('Sauna.count', -1) do
      delete :destroy, id: @sauna
    end

    assert_redirected_to saunas_path
  end
end
