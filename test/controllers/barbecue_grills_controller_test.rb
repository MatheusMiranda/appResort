require 'test_helper'

class BarbecueGrillsControllerTest < ActionController::TestCase
  setup do
    @barbecue_grill = barbecue_grills(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:barbecue_grills)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create barbecue_grill" do
    assert_difference('BarbecueGrill.count') do
      post :create, barbecue_grill: { date: @barbecue_grill.date, idUser: @barbecue_grill.idUser }
    end

    assert_redirected_to barbecue_grill_path(assigns(:barbecue_grill))
  end

  test "should show barbecue_grill" do
    get :show, id: @barbecue_grill
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @barbecue_grill
    assert_response :success
  end

  test "should update barbecue_grill" do
    patch :update, id: @barbecue_grill, barbecue_grill: { date: @barbecue_grill.date, idUser: @barbecue_grill.idUser }
    assert_redirected_to barbecue_grill_path(assigns(:barbecue_grill))
  end

  test "should destroy barbecue_grill" do
    assert_difference('BarbecueGrill.count', -1) do
      delete :destroy, id: @barbecue_grill
    end

    assert_redirected_to barbecue_grills_path
  end
end
