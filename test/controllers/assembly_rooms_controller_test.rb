require 'test_helper'

class AssemblyRoomsControllerTest < ActionController::TestCase
  setup do
    @assembly_room = assembly_rooms(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:assembly_rooms)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create assembly_room" do
    assert_difference('AssemblyRoom.count') do
      post :create, assembly_room: { date: @assembly_room.date, idUser: @assembly_room.idUser }
    end

    assert_redirected_to assembly_room_path(assigns(:assembly_room))
  end

  test "should show assembly_room" do
    get :show, id: @assembly_room
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @assembly_room
    assert_response :success
  end

  test "should update assembly_room" do
    patch :update, id: @assembly_room, assembly_room: { date: @assembly_room.date, idUser: @assembly_room.idUser }
    assert_redirected_to assembly_room_path(assigns(:assembly_room))
  end

  test "should destroy assembly_room" do
    assert_difference('AssemblyRoom.count', -1) do
      delete :destroy, id: @assembly_room
    end

    assert_redirected_to assembly_rooms_path
  end
end
