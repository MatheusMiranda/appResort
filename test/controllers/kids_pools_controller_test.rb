require 'test_helper'

class KidsPoolsControllerTest < ActionController::TestCase
  setup do
    @kids_pool = kids_pools(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:kids_pools)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create kids_pool" do
    assert_difference('KidsPool.count') do
      post :create, kids_pool: { date: @kids_pool.date, idUser: @kids_pool.idUser }
    end

    assert_redirected_to kids_pool_path(assigns(:kids_pool))
  end

  test "should show kids_pool" do
    get :show, id: @kids_pool
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @kids_pool
    assert_response :success
  end

  test "should update kids_pool" do
    patch :update, id: @kids_pool, kids_pool: { date: @kids_pool.date, idUser: @kids_pool.idUser }
    assert_redirected_to kids_pool_path(assigns(:kids_pool))
  end

  test "should destroy kids_pool" do
    assert_difference('KidsPool.count', -1) do
      delete :destroy, id: @kids_pool
    end

    assert_redirected_to kids_pools_path
  end
end
